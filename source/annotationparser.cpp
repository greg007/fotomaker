/*
annotationParser - vstup - nazev slozky
                                 - annotation.txt
                                 - int kernel
                                 - nazev dest
    - fce: cte postupne radky z annotation.txt a pro kazdy vola lineParser


*/

#include "annotationparser.h"
#include "lineparser.h"
#include "error_class.h"

annotationParser::annotationParser(){}

int lineCounter(string annotation){
    ifstream ff (annotation.c_str(), ifstream::in);
    if (!ff){
        error_class *error_obj = new error_class();
        (*error_obj).print_err_msg((*error_obj).READ_ANNOT_FILE_PARSER);
        delete error_obj;
        return -1;
    }
    string line;
    int lines = 1;

    while (getline(ff,line)){
        lines++;
    }

    // uzavreni streamu
    ff.close();
    return lines;
}

/*
 * cte postupne radky z annotation.txt a pro kazdy vola lineParser
 *
 * source - zdroj obrazku
 * annotation - adresa anotacniho souboru
 * kernel - velikost jadra pro lanczos
 * dest - cil ukladani obrazku
 */
int annotationParser::parse(string source, string annotation, string dest, vector<int> kernel, string source2, vector<int> picSizes){
    int result;
    int lines = lineCounter(annotation);

    // otevreni streamu
    ifstream ff (annotation.c_str(), ifstream::in);
    if (!ff){
        error_class *error_obj = new error_class();
        (*error_obj).print_err_msg((*error_obj).READ_ANNOT_FILE_PARSER);
        delete error_obj;
        return -1;
    }
    string line;
    lineParser lp;

    lp.fillIfIsEmptyFolderVector(picSizes);
    if ( lp.createFolderTree(dest, kernel, picSizes) != 0 ){
        return -1;
    }

    // postupne cteni vsech radku souboru
    int i = 0;
    cout << "processed................0%" << endl;
    while (getline(ff,line)){
        result = lp.parse(source, line, kernel, dest, source2, picSizes);
        lp.disposeVector();
        if ( result != 0 ){
            return -1;
        }

        i++;
        int proc1 = (int)((double)(i+1)/lines*100);
        int proc2 = (int)((double)i/lines*100);
        if (proc1/10 != proc2/10){
            cout << "processed................" << proc1 << "%" << endl;
        }
    }

    // uzavreni streamu
    ff.close();
    return 0;
}
