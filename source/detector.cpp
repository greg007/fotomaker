#include "detector.h"
#include "error_class.h"

Detector::Detector()
{
}

int Detector::detectAnnotation(string source, string dest, vector<int> kernel, vector<int> picSizes){
    return detectAnnotation(source, dest, "" , kernel, picSizes);
}

int Detector::detectAnnotation(string source, string dest, string source2 , vector<int> kernel, vector<int> picSizes){
    string filepath;
    DIR *dp;
    struct dirent *dirp;

    string realpath = source+source2;

    if ((dp = opendir(realpath.c_str())) == NULL){
        error_class *error_obj = new error_class();
        (*error_obj).print_err_msg((*error_obj).EXIST_SOURCE);
        delete error_obj;
        return -1;
    }

    // (int)dirp->d_type == 4 -> slozka
    while ((dirp = readdir(dp))){
        string file_name = dirp->d_name;
        filepath = source2 + "/" + file_name;
        if ((int)dirp->d_type == 4){
            if (file_name != "." && file_name != ".."){
                this->detectAnnotation(source,dest,source2+"/"+file_name,kernel, picSizes);
            }
        }
    }

    cout << realpath << endl;
    int err;
    if (detectedAnnotation(source, source2)){
        string anot = source+source2+"/annotation.txt";
        err = with_annot(source, anot, dest, source2, kernel, picSizes);
    }else{
        err = without_annot(source, dest, source2, kernel, picSizes);
    }

    closedir( dp );
    return err;
}

bool Detector::detectedAnnotation(string source, string source2){
    string annotation = source+source2+"/annotation.txt";
    ifstream ff (annotation.c_str(), ifstream::in);
    if (!ff){
        return false;
    }
    ff.close();
    return true;
}

int Detector::with_annot(string source, string annot, string dest, string source2, vector<int> kernel, vector<int> picSizes){
    annotationParser ap;
    if (ap.parse(source, annot, dest, kernel, source2, picSizes) == -1){
        return -1;
    }
    return 0;
}

int Detector::without_annot(string source, string dest, string source2, vector<int> kernel, vector<int> picSizes){
    annotationFinder af;
    if (af.find_annot(source,dest,kernel, source2, picSizes) == -1){
        return -1;
    }
    return 0;
}
