
/*
annotationParser - vstup - nazev slozky
                                 - annotation.txt
                                 - int kernel
                                 - nazev dest
    - fce: cte postupne radky z annotation.txt a pro kazdy vola lineParser


*/
#ifndef ANNOTATIONPARSER_H
#define ANNOTATIONPARSER_H

#include <string>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class annotationParser
{
public:
    annotationParser();
    /*
     * source - zdrojova slozka s obrazky
     * anotation - adresa souboru s anotacemi
     * kernel - velikost jadra pro lanczose
     * dest - cilova slozka pro ulozeni vysledku
     */
    int parse(string source, string annotation, string dest, vector<int> kernel, string source2, vector<int> picSizes);
};

#endif // ANNOTATIONPARSER_H
