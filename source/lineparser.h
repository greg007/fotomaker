/*
lineParser - vstup - string line
                     - int kernel
                     - nazev dest
    - fce: rozparsuje radek, precne nazev souboru - nacte obrazek, postupne pro
kazdy ctverecek vola cutSquare,
             ziskany xichtik v cyklu(pro 24, 26, 28 ...) posle do
lanc/near/bilin - toto vrati prescalovany obrazek, ulozime ho do dest

*/

#ifndef LINEPARSER_H
#define LINEPARSER_H

#include <string>
#include <vector>
#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "cutsquare.h"
#include "bilinear.h"
#include "nearest.h"
#include "lanczos.h"
#include "error_class.h"

#define COORDS_W 1      // coordinaty ve formatu x, y, x, y - typ anotace s mezerami  // zamysleno formatu x, y, w, h
#define COORDS_X 2      // coordinaty ve formatu x, y, x, y - typ anotace se strednikem

//todo /. dodelat do parametru vyber slozky
#define SCALESIZE_START 24
#define SCALESIZE_END   52
#define SCALESIZE_PLUS  4

#define BILINEARSCALE   11
#define NEARESSCALE     12
#define LANCZOSSCALE    13

using namespace std;
using namespace cv;

typedef struct {
    int x;
    int y;
    int w;
    int h;
}OneCoordSquare;

class lineParser
{
public:
    lineParser();
    /*
    rozparsuje radek, precne nazev souboru - nacte obrazek, postupne pro kazdy ctverecek vola cutSquare,
    ziskany xichtik v cyklu(pro 24, 26, 28 ...) posle do lanc/near/bilin - toto vrati prescalovany obrazek,
    ulozime ho do dest

     * source - zdrojova slouzka obrazku
     * line - retezec anotace k obrazku
     * kernel - velikost jadra pro lanczos
     * dest - cil ulozeni obrazku
     * flag - priznak tvaru anotacni radky - ocekava - ALL_PICTURES / ONE_PICTURE
    */
    void disposeVector();
    void fillIfIsEmptyFolderVector(vector<int>& picSizes );
    int parse(string source, string line, vector<int> kernel, string dest, string source2, vector<int> picSizes);
    int createFolderTree(string dest, vector<int> kernel, vector<int> picSizes);
    vector<OneCoordSquare*> getVector();


private:
    bool createSubfolders(string folder, vector<int> picSizes);
    bool createOrExistFolder(string dest);
    int checkAnnotationType(string line);
    int checkImgBorder( int x_y, int w_h, int c_r);
    int getAdaptedBorder( int coord, int length, int imgSize );
    int createSubfilesByDir(string files, string dest, string slash);
    int proccesSquareScaling(vector<int> kernel, string dest, string files, vector<int> picSizes);
    int saveImgIntoFolder (string dest, Mat newFaceBil, int newFaceSize, int scaleMethod , int kernel, string files);
    void checkFolderString(string& destWay);
    void addBorder(OneCoordSquare* square, int border);
    void checkCoordinates( OneCoordSquare* square, int type );
    void fillSquareWithNewCoords( OneCoordSquare* square, int border);
    void fillImgCoordVector(size_t endOfName, string line, string separator, int type);
    void writeAnnotation(string dest, string uniqName, int coordX, int coordY, int type);
    string getOneCoordFromLine(string line, string separator, size_t& lastSeparator , int &seqNum);
    string createDestWay(string dest, string slash, int newFaceSize, int scaleMethod, int kernel);
    string createAnnot(string separator, int coordX, int coordY);
    string createBaseWay(string dest, int type);
    string getUniqNameForStorage(string dest);
    string getBaseFolder(int scaleMethod);
    string intToString(int number);


private:
    vector<OneCoordSquare*> coordinates;
    string imgName;
    Mat img;
};

#endif // LINEPARSER_H
