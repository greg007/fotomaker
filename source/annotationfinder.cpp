/*
annotationFinder - vstup - vektor s obrazky
                                 - nazev slozky
                                 - int kernel
                                 - nazev dest
    - fce: postupne bere prvky vektoru, prida k nemu .txt - precte soubor s anotaci ( = 1 radek ) a obsah
vlozi do stringu a posle do lineParseru


*/

#include "annotationfinder.h"
#include "dirreader.h"
#include "error_class.h"
#include "lineparser.h"


annotationFinder::annotationFinder()
{
}


int annotationFinder::find_annot(string source, string dest, vector<int> kernel, string source2, vector<int> picSizes){
    int result;
    string realpath = source+"/"+source2;
    // ziskani seznamu obrazku
    dirReader dr;
    int ret = dr.dirParse(realpath);
    if (ret!=0)
        return ret;

    lineParser lp;
    lp.fillIfIsEmptyFolderVector(picSizes);
    if ( lp.createFolderTree(dest, kernel, picSizes) != 0 ){
        return -1;
    }

    cout << "processed................0%" << endl;
    for (int i = 0 ; i < dr.getSize() ; i++){
        // otevreni souboru s anotaci
        string txt = ".txt";
        ifstream ff ((dr.getPic(i)+txt).c_str(), ifstream::in);
        if (!ff){
            error_class *error_obj = new error_class();
            (*error_obj).print_err_msg((*error_obj).READ_ANNOT_FILE_FINDER);
            delete error_obj;
            cerr << " - file "<< dr.getPic(i)+txt <<" not found" << endl;
            continue;
        }

        // nacteni radku souboru
        string line;
        getline(ff,line);

        // odeslani radku lineParseru
        result = lp.parse(source, line, kernel, dest, source2, picSizes);
        lp.disposeVector();
        // uzavreni streamu
        ff.close();
        if ( result != 0 ){
            return -1;
        }

        int proc1 = (int)((double)(i+1)/dr.getSize()*100);
        int proc2 = (int)((double)i/dr.getSize()*100);
        if (proc1/10 != proc2/10){
            cout << "processed................" << proc1 << "%" << endl;
        }
    }
    return 0;
}
