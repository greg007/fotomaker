#include <iostream>
#include <cstdlib>
#include <string>
#include <cstdio>
#include <vector>
#include "detector.h"

/*
main - vstup - nazev slozky
                 - nazev dest
    - fce - vytvori adresarovou strukturu pro ukladani obrazku - results( lanczos, bilinear, nearest ) - + kazda podslozka obsahuje jeste slozky podle velikosti obrazku ( 24, 28, 32, .... )
            - if - zadana annotation.txt -> vola annotationParser
            - else - zavola dirReader - ziska seznam obrazku ve slozce, zavola annotationFinder


*/

using namespace std;

void help();
int getNumbersFromParam(vector<int>& vector, string param, string keyWord );


void help(){
    string hh =
    "HELP: sousteni programu\n\
    ./progam source destination LANC SIZES\n\
    \n\
    source :\t zdrojova slozka s obrazkama\n\
    destination :\t cilova slozka pro ulozeni obrazku\n\
    LANC=kernel,kernel,... :\t velikost kernelu k metode Lanczos, velikosti jader oddelujte carkami bez mezer\n\
    SIZES=size,size,... :\t velikosti vyslednych objektu v obrazku, pokud se tento parametr nezada, vytvori se obrazky vsech velikosti\n\
    \n\
    napr.: ./program ~/Plocha/files ~/Plocha/result LANC=2,3,5,7 SIZES=24,32,52\n\
    ";
    cout << hh<< endl;
}

int getNumbersFromParam(vector<int>& vector, string param, string keyWord)
{
    if ( param.find_first_of(keyWord) != 0 ){
        help();
        return -1;
    }else {
        size_t eq = param.find_first_of("=");
        size_t last;
        string number;
        string separ = ",";

        if ( param.size()-1 != param.find_last_of(separ)){
            param.append(separ);
        }

        last = param.find_first_of(separ);

        while ( last != string::npos ){
            number = param.substr( eq+1, ( last - eq - 1 ));
            eq = last;
            last = param.find(separ, eq+1);
            vector.push_back(atoi(number.c_str()));
        }
    }

    return 0;
}

int main(int argc, char** argv)
{
    if (( argc == 4 ) || ( argc == 5 )){
        Detector* det = new Detector();
        int res = 0;
        vector<int> kernel;
        vector<int> picSizes;

        if ( getNumbersFromParam(kernel, argv[3], "LANC=") == 0 ){
            if ( argc == 5 ){
                if ( getNumbersFromParam(picSizes, argv[4], "SIZES=") != 0 ){
                    return 0;
                }
            }

            res = det->detectAnnotation( argv[1], argv[2], kernel, picSizes );
        }

        delete (det);
        return res;
    }else {
        help();
    }

    return 0;
}

