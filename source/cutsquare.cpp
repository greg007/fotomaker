/*
cutSquare - vstup - obrazek
                        - int x, int y, int width, int height
             - vystup - img  - vyrezana cast z puvodniho obrazku

 */


#include "cutsquare.h"
#include "lanczos.h"
#include <iostream>

cutSquare::cutSquare()
{
}


Mat cutSquare::getSquare(Mat source, int x, int y, int width, int height){
    /*std::cout << "x:" << x;
    std::cout << "y:" << y;
    std::cout << "w:" << width;
    std::cout << "h:" << height;
    std::cout << "r:" << source.rows;
    std::cout << "c:" << source.cols << std::endl;*/
    Mat vyrez (width, height, source.type());
    lanczos lanc;
    for (int xx = 0 ; xx < width ; xx++){
        for (int yy = 0 ; yy < height ; yy++){
            unsigned cc = lanc.get_mirrored_repeat_pos(source.cols,x+xx);
            unsigned rr = lanc.get_mirrored_repeat_pos(source.rows,y+yy);
            source.col(cc).row(rr).copyTo(vyrez.col(xx).row(yy));
        }
    }
    return vyrez;
}
