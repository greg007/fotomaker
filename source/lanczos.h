/*
lanczos - vstup - uz orezany obrazek obliceje
                     - int velikost_vysledku (24. 26. 28 ...)
                     - int kerlnel (velikost - 2,3,5, ... )
         - vystup - prescalovany obrazek


 */

#ifndef LANCZOS_H
#define LANCZOS_H


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

typedef struct
  {
    std::vector<float> data; // coeficients to multiply old image values by (various sizes)
    int shift; // shift in pixels relative to new image / old image filter start
  }s_filter;

typedef std::vector<s_filter> v_filter;


class lanczos
{
public:
    lanczos();

    /*
     * image - vstupni obrazek
     * pict_width - sirka obrazku (24, 26, 28,...)
     * pict_height - vyska obrazku (24, 26, 28,...)
    */
    Mat scale(Mat image, int pict_width, int pict_height, int kernel);
    /*
     * vraci zrdcadleny obrazek (hlavne co se tyce tech okraju)
     */
    unsigned int get_mirrored_repeat_pos(unsigned int max_size, int act_pos);
private:

    void lancos(Mat input, Mat & output, int posx, int posy, Size size, int kernel);
    v_filter create_filter(unsigned int resize_from, unsigned int resize_to, unsigned int kernel_size);
};

#endif // LANCZOS_H
