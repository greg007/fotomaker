/*
lanczos - vstup - uz orezany obrazek obliceje
                     - int velikost_vysledku (24. 26. 28 ...)
                     - int kerlnel (velikost - 2,3,5, ... )
         - vystup - prescalovany obrazek


 */


#include "lanczos.h"
#define PI 3.1415

lanczos::lanczos(){}

Mat lanczos::scale(Mat image, int pict_width, int pict_height, int kernel){
    Size velikost (pict_width*3, pict_height*3); // ramecek ma stejnou sirku jako obrazek
    Mat output;
    lancos(image, output, image.cols/3, image.rows/3, velikost, kernel);
    return output;
}

void lanczos::lancos(Mat input, Mat & output, int posx, int posy, Size size, int kernel){
    // alokace vysledne matice
    //Mat result;
    //cv::resize(input, result, size, 0, 0, INTER_LANCZOS4);
    Mat result (size.width, size.height, input.type());
    size.width = size.width/3;
    size.height= size.height/3;
    Mat buffer (1, 1, input.type());

    v_filter filters = create_filter(input.rows,size.height*3,kernel);

    for(int m = -1; m <= 1; m++) // vyber casti obrazu (horni okraj, objekt, spodni okraj)
        for(int n = -1; n <= 1; n++) // vyber casti obrazu (levy okraj, objekt, pravy okraj)
            for(int k = 0; k < size.height; k++)  //vyber y souradnice vytvareneho pixelu
                for(int l = 0; l < size.width; l++){  //vyber x souradnice vytvareneho pixelu
                    float output_datar = 0.0f;
                    float output_datag = 0.0f;
                    float output_datab = 0.0f;
                    for(unsigned i = 0; i < filters[k].data.size(); i++) // vyber radku ve filtru
                        for(unsigned j = 0; j < filters[l].data.size(); j++){ // vyber sloupce ve filtru
                            // pronasobeni hodnot filtru pro dany pixel [k][l]
                            unsigned rr = get_mirrored_repeat_pos(input.rows, posy+filters[k].shift+i+m*input.rows/3);
                            unsigned cc = get_mirrored_repeat_pos(input.cols, posx+filters[l].shift+j+n*input.cols/3);
                            input.row(rr).col(cc).copyTo(buffer);
                            output_datar += (buffer.col(0).row(0).data[0] * filters[k].data[i] * filters[l].data[j]);
                            output_datag += (buffer.col(0).row(0).data[1] * filters[k].data[i] * filters[l].data[j]);
                            output_datab += (buffer.col(0).row(0).data[2] * filters[k].data[i] * filters[l].data[j]);
                        }
                    if (output_datar < 0) output_datar = 0;
                    if (output_datag < 0) output_datag = 0;
                    if (output_datab < 0) output_datab = 0;

                    if (output_datar > 255) output_datar = 255;
                    if (output_datag > 255) output_datag = 255;
                    if (output_datab > 255) output_datab = 255;

                    //cout << l+(n+1)*size.width << "/" << result.cols << "  " << k+(m+1)*size.height << "/" << result.rows << endl;
                    result.col(l+(n+1)*size.width).row(k+(m+1)*size.height).data[0] = output_datar; // zapsani dat do vysledneho obrazu
                    result.col(l+(n+1)*size.width).row(k+(m+1)*size.height).data[1] = output_datag; // zapsani dat do vysledneho obrazu
                    result.col(l+(n+1)*size.width).row(k+(m+1)*size.height).data[2] = output_datab; // zapsani dat do vysledneho obrazu
                }

    // predani vysledku na vystup
    output = result;
    buffer.release();
}

// get mirrored repeat position
// @param max_size act axis image size
// @param act_pos actual position
// @return mirrored repeat position
unsigned int lanczos::get_mirrored_repeat_pos(unsigned int max_size, int act_pos)
  {
    unsigned int act_abs_pos = abs(act_pos);
    unsigned int act_image_pos = act_abs_pos % max_size;
    return ((act_abs_pos / max_size) % 2 == 0) ? act_image_pos : max_size - 1 - act_image_pos;
  }

// position unit is original image pixels relative to new image's filter start
// @param resize_from original object size (pixels)
// @param resize_to transformed object size (pixels)
// @param kernel_size lanczos kernel size
// @return filters
v_filter lanczos::create_filter(unsigned int resize_from, unsigned int resize_to, unsigned int kernel_size)
  {
    float step_ratio = resize_from / (float)(resize_to); // resize factor
    int a = kernel_size; // filter size
    int min_pos, max_pos; // filter start and end positions in original image relative to new image/ old image start
    float middle_pos; // position in original image relative to new image/ old image start
    float Lx; // result value
    s_filter filter; // actual filter
    v_filter filters; // filters to return

    // calculate filter - i -> pixel in new image relative to new image/old image filter start
    for(unsigned int i = 0; i < resize_to; i++)
      {
        filter.data.clear();
        middle_pos = i * step_ratio;
        min_pos = (int)ceil((((int)i) - a) * step_ratio);
        max_pos = (int)((i + a) * step_ratio);

        float sum = 0.0f;
        int filter_size = max_pos - min_pos + 1;

        // calculate filter - j -> pixel in old image relative to i pixel in new image filtes start
        for(int j = 0; j < filter_size; j++)
          {
            float x = -middle_pos + j + min_pos;
            if(x == 0.0f)
              {
                Lx = 1.0f;
              }
            else
              {
                Lx = a * sin(x * PI) * sin(x * PI / a)/(PI * PI * x * x);
              }
            sum += Lx;
            filter.data.push_back(Lx);
          }
        // normalize filter
        for(unsigned int j = 0; j < filter.data.size(); j++)
          {
            filter.data[j] /= sum;
          }
        filter.shift = min_pos;
        filters.push_back(filter);
      }
    return filters;
  }
