/*
cutSquare - vstup - obrazek
                        - int x, int y, int width, int height
             - vystup - img  - vyrezana cast z puvodniho obrazku

 */

#ifndef CUTSQUARE_H
#define CUTSQUARE_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

class cutSquare
{
public:
    cutSquare();
    /*
     * source - vstupni obrazek
     * x,y - pozice ctverce pro vyrez obrazku
     * width,height - rozmery ctverce
     */
    Mat getSquare(Mat source, int x, int y, int width, int height);
};

#endif // CUTSQUARE_H
