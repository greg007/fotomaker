/*
dirReader - vstup - nazev slozky
             - vystup - seznam obrazku

*/

#include "dirreader.h"
#include "error_class.h"

/*
 * zmeni vsechny znaky retezce na mala pismena
 */
string strtolower(string in){
    string buff = "";
    for (unsigned int i = 0 ; i < in.size() ; i++){
        string c = " ";
        c.at(0) = (char)tolower(in.c_str()[i]);
        buff.append(c);
    }
    return buff;
}

/*
 * vraci true, pokud ma soubor koncovku obrazku (vsechny podporovane formaty funkce imread)
 */
bool pic_match(string s){
    string podret = s.substr(s.size()-4,4);
    vector<string> koncovky;
    koncovky.push_back(".bmp");
    koncovky.push_back(".dib");
    koncovky.push_back("jpeg");
    koncovky.push_back(".jpg");
    koncovky.push_back(".jpe");
    koncovky.push_back(".jp2");
    koncovky.push_back(".png");
    koncovky.push_back(".pbm");
    koncovky.push_back(".pgm");
    koncovky.push_back(".ppm");
    koncovky.push_back(".ras");
    koncovky.push_back("tiff");
    koncovky.push_back(".tif");

    for (unsigned i = 0 ; i < koncovky.size() ; i++){
        if (!strtolower(podret).compare(koncovky.at(i)))
            return true;
    }
    return false;
}

dirReader::dirReader(){}

int dirReader::dirParse(string source){
    string filepath;
    DIR *dp;
    struct dirent *dirp;

    if ((dp = opendir(source.c_str())) == NULL){
        error_class *error_obj = new error_class();
        (*error_obj).print_err_msg((*error_obj).EXIST_SOURCE);
        delete error_obj;
        return -1;
    }

    while ((dirp = readdir(dp))){
        filepath = source + "/" + dirp->d_name;
        if (pic_match(filepath)){
            this->pics.push_back(filepath);
        }
    }
    closedir( dp );
    return 0;
}


string dirReader::getPic(int index){
    return this->pics.at(index);
}


int dirReader::getSize(){
    return this->pics.size();
}

