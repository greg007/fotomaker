/*
lineParser - vstup - string line
                         - string source
                         - int kernel
                         - nazev dest
    - fce: rozparsuje radek, precne nazev souboru - nacte obrazek, postupne pro kazdy ctverecek vola cutSquare,
             ziskany xichtik v cyklu(pro 24, 26, 28 ...) posle do lanc/near/bilin - toto vrati prescalovany obrazek, ulozime ho do dest

*/


#include "lineparser.h"

lineParser::lineParser()
{
}

/**
 * @brief lineParser::parse - hlavni fuknce tridy, rozparsuje radek z anotace, nacte obrazek a vyreze prislusne obliceje,
 *          provede s nimi ruzna skalovani a ulozi je ro slozky posle nazvu pouzite metody a velikosti vysledneho obrazku
 * @param source - slozka s obrazky
 * @param line - 1 radek z anotacniho souboru
 * @param kernel - velikost jadra pro lanczose
 * @param dest - slozka pro umisteni slozkoveho stromu pro vysledne obrazky
 */
int lineParser::parse(string source, string line, vector<int> kernel, string dest, string source2, vector<int> picSizes){
    // pozn. pred volanim cutSqare.getSqare je potreba prepocitat velikost ramecku podle rozmeru
    // vyrezavani a velikosti vysledneho obrazku a pripocist ho k vyslednym rozmerum a pozici vyrezavaneho obrazku

    string separatorName, imgAllName, way = "", separator = " ";
    size_t endOfName;

    int type = checkAnnotationType(line);   // ted vzdy vraci COORDS_X - check typu coordinacniho systemu

    if ( type == COORDS_W ){
        separatorName = " ";
    } else {
        separatorName = ";";
        size_t founded = line.find_first_of(separatorName);
        if ( founded == std::string::npos ){
            separatorName = " ";
        }
    }

    endOfName = line.find_first_of(separatorName);
    imgAllName = line.substr(0, endOfName);

    this->imgName = imgAllName.substr( 0, imgAllName.find_last_of("."));

    checkFolderString(source);  // pridani lomitka na konec cest
    checkFolderString(source2);
    checkFolderString(dest);

    way = source;           // slozeni zdrojove cesty
    way.append(source2);
    way.append(imgAllName);

    this->img = imread( way, CV_LOAD_IMAGE_COLOR );     // nacteni obrazku
    if ( !this->img.data ){
        error_class *error_obj = new error_class();
        (*error_obj).print_err_msg((*error_obj).READ_IMAGE_FILE);
        cerr << "file: " << this->imgName << endl;
        delete error_obj;
        return 0;
    }

    if ( separatorName.compare(";") == 0 ){
        replace( line.begin(), line.end(), ',', ' ' );
        replace( line.begin(), line.end(), ';', ' ' );
    }

    fillImgCoordVector(endOfName, line, separator, type);   // naplneni struktury coordynaty

    if ( proccesSquareScaling( kernel, dest, source2, picSizes ) != 0 ){  // orezani a ulozeni obrazku
        return -1;
    }

    return 0;
}

/**
 * @brief lineParser::fillEmptyFolderVector - pokud nebyly zadany velikosti vyslednych obrazku, berou se defaultne vsechny
 *                      a tak se vektor s velikostmi naplni predem na vsechny velikosti. Funkce vraci naplneny vektor pres parametr.
 * @param picSizes
 */
void lineParser::fillIfIsEmptyFolderVector(vector<int>& picSizes ){
    if ( picSizes.empty() ){
        int pictureSizes = SCALESIZE_START;

        while ( pictureSizes <= SCALESIZE_END ){
            picSizes.push_back(pictureSizes);
            pictureSizes += SCALESIZE_PLUS;
        }
    }
}

/**
 * @brief lineParser::createFolderTree - vytvori slozkovou strukturu pro vytvorene vystupni obrazky
 * @param dest - slozka, ve kt. se bude struktura nachazet
 */
int lineParser::createFolderTree( string dest, vector<int> kernel, vector<int> picSizes ){

    string folder;

    checkFolderString(dest);
    folder = dest;

    // test zda existuje slozka pro ukladani obrazku
    if ( !createOrExistFolder(folder) ){
        return -1;
    }

    // vytvorim si 3 hlavni slozky - pro nearest, bilinear a lanczose a jejich podslozky
    folder = createBaseWay( dest, BILINEARSCALE);
    if ( !createSubfolders(folder, picSizes) ){
        return -1;
    }

    folder = createBaseWay( dest, NEARESSCALE);
    if ( !createSubfolders(folder, picSizes) ){
        return -1;
    }

    folder = createBaseWay( dest, LANCZOSSCALE);
    string file = folder;
    if ( !createOrExistFolder(folder) ){
        return -1;
    }
    for ( unsigned int i = 0; i < kernel.size(); i++ ){ // vytvoreni podlozek pro jednotlive velikosti jader
        folder = file;
        folder.append(intToString(kernel.at(i)));
        folder.append("/");
        if ( !createSubfolders(folder, picSizes) ){
            return -1;
        }
    }

    return 0;
}

/**
 * @brief lineParser::createBaseWay
 * @param dest
 * @param type
 * @return
 */
string lineParser::createBaseWay( string dest, int type){
    string folder = dest;
    folder.append(getBaseFolder(type));
    folder.append("/");
    return folder;
}

/**
 * @brief lineParser::createSubfolders - vytvori podslozky s nazvy velikosti obrazku
 * @param folder
 * @return
 */
bool lineParser::createSubfolders(string folder, vector<int> picSizes){

    string subfolder;

    if ( !createOrExistFolder(folder) ){
        return false;
    }

    for (unsigned int i = 0; i < picSizes.size(); i++){
        subfolder = folder;
        subfolder.append(intToString(picSizes.at(i)));
        subfolder.append("/");

        if ( !createOrExistFolder(subfolder) ){
            return false;
        }
    }

    return true;
}

/**
 * @brief lineParser::createFolder - fce vytvarejici podslozky - pokud existuje, vraci 0 = ok
 * @param dest
 * @return
 */
bool lineParser::createOrExistFolder(string dest){
    int flag = 0;
    if (( flag = mkdir(dest.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)) && ( errno != EEXIST)){
        error_class *error_obj = new error_class();
        (*error_obj).print_err_msg((*error_obj).DEST_FOLDER);
        delete error_obj;
        return false;
    }

    return true;
}


/**
 * @brief lineParser::proccesSquareScaling - fce nejprve oreze kazdy oblicej ze seznamu, pro kazdy orezany oblicej
 *      zavola kazdy typ scalovani na ruzne velikosti, vysledky pak ulozi do souboru
 * @param kernel - velikost jadra pro lancsoz
 * @param dest - slozka pro umisteni souboru
 */
int lineParser::proccesSquareScaling(vector<int> kernel, string dest, string files, vector<int> picSizes){
    cutSquare* cutSq = new cutSquare();
    bilinear* bilinScale = new bilinear();
    nearest* nearScale = new nearest();
    lanczos* lanczScale = new lanczos();

    int notLanc = -1;       // priznak ze dana metoda neni lancoz a tak se nepocita s dalsi podslozkou

    //cout << this->imgName << endl;

    for ( unsigned int i = 0; i < this->coordinates.size(); i++ ){
        // oriznuti 1 obliceje        

        Mat face = (*cutSq).getSquare(this->img, this->coordinates.at(i)->x, this->coordinates.at(i)->y,
                           this->coordinates.at(i)->w, this->coordinates.at(i)->h);
        if (face.cols == 0){
            cout << "<-- u predchozich souradnic neco je blbe "<< this->imgName << endl;
            continue;
        }

        // cyklus pro naskalovani na pozadovane velikosti + ulozeni do prislusne slozky
        for ( unsigned int e = 0; e < picSizes.size(); e++ ){
            // NOTE: pokud je treba program prepsat na to aby vysledkem byl obdelnik a ne ctverec, pak zde se musi
            //       picSizes.at(e) druhy parametr nahradit druhou velikosti obdelniku -> tzn bylo by vhodne vektor picSizes
            //       prepsat tak aby polozka obsahovala dvojici cisel - sirku a vysku obdelniku a tyto udaje pak zadavat sem
            Mat newFaceBil = bilinScale->scale(face, picSizes.at(e), picSizes.at(e) );
            if ( saveImgIntoFolder ( dest, newFaceBil, picSizes.at(e), BILINEARSCALE, notLanc, files ) != 0 ){
                return -1;
            }

            Mat newFaceNear = nearScale->scale(face, picSizes.at(e), picSizes.at(e) );
            if ( saveImgIntoFolder ( dest, newFaceNear, picSizes.at(e), NEARESSCALE, notLanc, files ) != 0 ){
                return -1;
            }

            for ( unsigned int u = 0; u < kernel.size(); u++ )
            { // spusteni lancosce pro 1 obrazek se vsemi pozadovanymi velikostmi jadra
                Mat newFaceLanc = lanczScale->scale(face, picSizes.at(e), picSizes.at(e), kernel.at(u) );
                if ( saveImgIntoFolder ( dest, newFaceLanc, picSizes.at(e), LANCZOSSCALE, kernel.at(u), files ) != 0 ){
                    return -1;
                }
            }
        }

    }

    delete(lanczScale);
    delete(nearScale);
    delete(bilinScale);
    delete(cutSq);

    return 0;
}

/**
 * @brief lineParser::createAnnot - vytvori anotaci pro orezany oblicej
 * @param separator
 * @param coord
 * @return
 */
string lineParser::createAnnot(string separator, int coordX, int coordY){

    string name = intToString(coordX);
    name.append(separator);
    name.append(intToString(coordY));
    name.append(separator);
    name.append(intToString(coordX*2));
    name.append(separator);
    name.append(intToString(coordY*2));

    return name;
}

/**
 * @brief lineParser::writeAnnotation - ulozi jednotlivou anotaci k fotce
 * @param dest
 */
void lineParser::writeAnnotation(string dest, string uniqName, int coordX, int coordY, int type ){
    FILE * pFile;
    string textFile = dest; // dest uz obrahuje na konci jmeno souboru
    string annot = uniqName;
    string mode;

    if ( type == COORDS_X ){    // typ unikatni anotace pro 1 obrazek 1 jednoradkova anotace
        mode = "w";
        annot.append(";");
        annot.append(createAnnot(",", coordX, coordY));
        textFile.append(uniqName);
    }else {     // celkova anotace - s mezerami, pripise se na konec souboru annotation.txt
        mode = "a";
        annot.append(" ");
        annot.append(createAnnot(" ", coordX, coordY));
        annot.append("\n");
        textFile.append("annotation");
    }

    textFile.append(".txt");

    pFile = fopen(textFile.c_str(), mode.c_str());    // otevreni/vytvoreni souboru

    if (pFile!=NULL) {
        fputs (annot.c_str(),pFile);        // zapis anotace
        fclose (pFile);
    }
}

/**
 * @brief lineParser::createSubfilesByDir - fce postupne bere kazdou slozku ze stringu files ktery rika, jake zanoreni a v jakych
 *              slozkach ma na danem miste byt. Vytvori danou slozku (nebo testne jestli existuje) a postupuje retezcem az na konec.
 * @param files - nove podslozky
 * @param dest - cilova slozka
 * @param slash - oddelovac slozek
 * @return
 */
int lineParser::createSubfilesByDir(string files, string dest, string slash){
    size_t first, last;
    string newFolder = dest;

    first = files.find_first_of(slash); // najde 1. slash
    if ( first != 0 ){              // nenachazi se na zacatku cesty -> vnutime index 0 aby zacal na zacatku
        first = 0;
    }

    while ( first != string::npos ){
        last = files.find_first_of(slash, first+1);

        newFolder.append( files.substr( first, last-first) );
        if ( !createOrExistFolder(newFolder) ){
            return -1;
        }

        first = files.find_first_of(slash, last);
    }

    return 0;
}


/**
 * @brief lineParser::saveImgIntoFolder
 * @param dest
 * @param img
 * @param newFaceSize
 * @param scaleMethod
 */
int lineParser::saveImgIntoFolder ( string dest, Mat img, int newFaceSize, int scaleMethod, int kernel, string files ){
    string slash = "/";

    string destWay = createDestWay(dest, slash, newFaceSize, scaleMethod, kernel);

    if ( !files.empty() ){          // existuje specificka podslozka -> vytvorit
        if ( createSubfilesByDir(files, destWay, slash) != 0 ){
            return -1;
        }
    }

    if ( files.find_first_of(slash) == 0 ){
        destWay.append(files.substr(1));    // oriznuti jednoho slashe -> 1 znak na zacatku retezce
    }else {
        destWay.append(files);
    }

    string uniqName = getUniqNameForStorage(destWay);   //vytvori unikatni jmeno pro danou fotku v dane slozce

    int coordX = img.rows/3;
    int coordY = img.cols/3;

    writeAnnotation(destWay, uniqName, coordX, coordY, COORDS_X);      // vytvori fotce anotaci unikatni - jednoradkova
    writeAnnotation(destWay, uniqName, coordX, coordY, COORDS_W);  // prida anotaci do celkoveho souboru s anotacemi

    destWay.append(uniqName);

    vector<int> pngParams;
    pngParams.push_back(CV_IMWRITE_PNG_COMPRESSION);
    pngParams.push_back(0);

    try {
        imwrite( destWay, img, pngParams);              // ulozi obrazek
    }
    catch(Exception& e) {
        error_class *error_obj = new error_class();
        (*error_obj).print_err_msg((*error_obj).WRITE_IMAGE_FILE);
        delete error_obj;
        return 0;
    }

    return 0;
}

/**
 * @brief lineParser::checkFolderString - fce na hlidani lomitek na konci cesty
 * @param destWay
 * @return
 */
void lineParser::checkFolderString( string& destWay ){
    if ( destWay.find_last_of("/") != (destWay.size()-1) ){
        destWay.append("/");
    }
}

/**
 * @brief lineParser::getBaseFolder - podle pouzite metody vrati nazev hlavni slozky
 * @param scaleMethod - typ metody
 * @return
 */
string lineParser::getBaseFolder(int scaleMethod){
    switch(scaleMethod){
        case BILINEARSCALE:
            return "bilinear_scale";
        case NEARESSCALE:
            return  "nearestNeighbour_scale";
        case LANCZOSSCALE:
            return "lanczos_scale";
        default:
            return "";
    }
}

/**
 * @brief lineParser::getUniqNameForStorage - funkce ktera vytvori unikatni nazev pro oblicej - zkousi postupne
 *              pristup ke slozkam s danym jmenem a prvni volne vraci
 * @param dest - cesta do cilove slozky
 * @return
 */
string lineParser::getUniqNameForStorage(string dest){
    string name, file;
    string end = ".png";
    int counter = 0;
    bool exist = true;

    while (exist){
        counter++;
        file = dest;
        name = this->imgName;
        name.append("_");
        name.append(intToString(counter));
        name.append(end);
        file.append(name);
        exist = ( access( file.c_str(), F_OK ) != -1 );
    }

    return name;
}

/**
 * @brief lineParser::createDestWay
 * @param dest - slozka pro ulozeni obrazku
 * @param slash - druh lomitka
 * @param newFaceSize - velikost noveho obrazku = podslozka
 * @param scaleMethod - nazev metody = slozka
 * @return
 */
string lineParser::createDestWay(string dest, string slash, int newFaceSize, int scaleMethod, int kernel){
    string destWay = dest;

    checkFolderString( destWay );

    destWay.append(getBaseFolder(scaleMethod));
    destWay.append(slash);
    if ( kernel != -1 ){
        destWay.append(intToString(kernel));
        destWay.append(slash);
    }
    destWay.append(intToString(newFaceSize));
    destWay.append(slash);

    return destWay;
}

/**
 * @brief lineParser::intToString
 * @param number - cislo ktere se ma prevest na string
 * @return
 */
string lineParser::intToString(int number)
{
   stringstream new_string;
   new_string << number;
   return new_string.str();
}


/**
 * @brief lineParser::getOneCoordFromLine
 * @param line - radek ze souboru annotation
 * @param separator - oddelovac coordinatu - posle typu anotace
 * @param lastSeparator - index posledniho nalezeneho oddelovace
 * @param seqNum - cislo ziskavaneho coordinatu v sekvenci
 * @return
 */
string lineParser::getOneCoordFromLine(string line, string separator, size_t& lastSeparator, int& seqNum){
    string coord;
    size_t newLastSeparator = line.find(separator, lastSeparator+1);

    if ( newLastSeparator != string::npos ){
        coord = line.substr( lastSeparator+1, ( newLastSeparator - lastSeparator - 1 ));
    }else {
        if ( seqNum % 4 == 0 ){     // musi byt 4 coordinaty pro kazdy ctverec, pri poslednim chybi oddelovac
            coord = line.substr( lastSeparator+1 );
        }else {
            error_class *error_obj = new error_class();
            (*error_obj).print_err_msg((*error_obj).MISTAKE_ANNOT_FILE);
            delete error_obj;
            return "--";
        }
    }

    lastSeparator = newLastSeparator;
    seqNum++;

    return coord;
}

/**
 * @brief lineParser::fillImgCoordVector
 * @param endOfName - pozice oddelovace nazvu souboru od coordinatu
 * @param line - radek s anotaci
 * @param separator - oddelovac coordinatu
 * @param type - typ anotace
 */
void lineParser::fillImgCoordVector(size_t endOfName, string line, string separator, int type){
    size_t lastSeparator = endOfName;
    int seq = 1;

    while ( lastSeparator != string::npos ){
        OneCoordSquare* square = new OneCoordSquare();
        square->x = atoi( getOneCoordFromLine(line, separator, lastSeparator, seq).c_str() );
        square->y = atoi( getOneCoordFromLine(line, separator, lastSeparator, seq).c_str() );
        square->w = atoi( getOneCoordFromLine(line, separator, lastSeparator, seq).c_str() );
        square->h = atoi( getOneCoordFromLine(line, separator, lastSeparator, seq).c_str() );

        checkCoordinates( square, type );         // check zda maji coordinaty spravny tvar
        addBorder( square, square->w );
        this->coordinates.push_back(square);
    }
}

/**
 * @brief lineParser::addBorder - funkce zmeni hodnoty ve strukture predavane v parametru. K hodnotam prida ramecek pro dany
 *          objekt, ktery ma - pokud to jde - velikost sirky objektu. Pokud to nelze, upravi se sirka ramecku - zmensi se
 *          na maximalni moznou velikost ktera vyhovuje.
 * @param square - struktura s coordinaty
 */
void lineParser::addBorder( OneCoordSquare* square, int border ){
    square->x = square->x - border;
    square->y = square->y - border;
    square->h = square->h + 2*border;
    square->w = square->w + 2*border;
}


/**
 * @brief lineParser::checkCoordinates - funkce na osetreni chybovych stavu coordinatu a na jejich korekci.
 * @param square
 * @param type
 * @return
 */
void lineParser::checkCoordinates( OneCoordSquare* square, int type ){

    if ( type == COORDS_X ){            // anotace x, y, x, y -> prepocet na x, y, w, h
        square->w = square->w - square->x;
        square->h = square->h - square->y;
    }

    if ( square->w != square->h ){      // musi byt stejna vyska i sirka obrazku
        if ( square->w > square->h ){
            int halfDiff = (square->w - square->h) / 2;
            square->h = square->w;
            square->y = square->y - halfDiff;
        }else {
            int halfDiff = (square->h - square->w) / 2;
            square->w = square->h;
            square->x = square->x - halfDiff;
        }
    }
}

/**
 * @brief lineParser::checkAnnotationType
 * @param line - radek anotace
 * @return vraci priznak typu anotace
 */
int lineParser::checkAnnotationType(string line){

    size_t withSemic = line.find(";");

    if ( withSemic != string::npos ){
        return COORDS_X;        // strednik byl nalezenen
    } else {
        //return COORDS_W;
        return COORDS_X;
    }
}

/**
 * @brief lineParser::disposeVector - funkce na smazani vektoru s koordinaty
 */
void lineParser::disposeVector(){
    int size = this->coordinates.size();
    for ( int i = size-1; i >= 0; i-- ){
        OneCoordSquare* square = this->coordinates.at(i);
        delete( square );
        this->coordinates.pop_back();
    }
}

vector<OneCoordSquare *> lineParser::getVector(){
    return this->coordinates;
}
