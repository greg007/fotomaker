/*
nearest - vstup - uz orezany obrazek obliceje
                     - int velikost_vysledku (24. 26. 28 ...)
         - vystup - prescalovany obrazek

 */


#include "nearest.h"

nearest::nearest(){}

Mat nearest::scale(Mat image, int pict_width, int pict_height){
    Size velikost (pict_width*3, pict_height*3); // ramecek ma stejnou sirku jako obrazek
    Mat output;
    resize(image,output,velikost,0,0,INTER_NEAREST);
    return output;
}
