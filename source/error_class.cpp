#include "error_class.h"

/** Konstruktor tridy pro chybova hlaseni. **/
error_class::error_class()
{}


/** Pole udrzujici chybove hlasky. **/
const char *error_class::ERR_STR[] =
{
    "Error: reading annotation file - file annotation.txt not found",
    "Error: reading annotation file",
    "Error: annotation syntax",
    "Error: opening source directory",
    "Error: can not open or create destination directory",
    "Error: writing image file",
    "Error: reading image file"
};

/** Funkce realizujici vypis chybovych hlasek pro cely program. **/
void error_class::print_err_msg( int enum_code )
{
    if (( enum_code >= READ_ANNOT_FILE_PARSER ) && ( enum_code <= READ_IMAGE_FILE )) {
        std::cerr << ERR_STR[enum_code] << std::endl;
    }
    else {
        std::cerr << "Unknown error." << std::endl;
    }
}
