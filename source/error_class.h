#include <iostream>

#ifndef ERROR_CLASS_H
#define ERROR_CLASS_H

/** Trida na vypis chybovych hlaseni.
  * Chybove retezce se udrzuji v konstantnim poli, vypisuji se pri
  * zachyceni vyjimky. **/
class error_class
{
    public:
        error_class();
        void print_err_msg( int enum_code );

        static const char *ERR_STR[];
        enum codes_for_errmsg
        {
            READ_ANNOT_FILE_PARSER,
            READ_ANNOT_FILE_FINDER,
            MISTAKE_ANNOT_FILE,
            EXIST_SOURCE,
            DEST_FOLDER,
            WRITE_IMAGE_FILE,
            READ_IMAGE_FILE
        };
};

#endif // ERROR_CLASS_H
