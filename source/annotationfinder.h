/*
annotationFinder - vstup - vektor s obrazky
                                 - nazev slozky
                                 - int kernel
                                 - nazev dest
    - fce: postupne bere prvky vektoru, prida k nemu .txt - precte soubor s anotaci
 ( = 1 radek ) a obsah vlozi do stringu a posle do lineParseru


*/

#ifndef ANNOTATIONFINDER_H
#define ANNOTATIONFINDER_H

#include <string>
#include <vector>
#include <iostream>

using namespace std;

class annotationFinder
{
public:
    annotationFinder();
    /*
     * source - zdrojova slozka s obrazky
     * kernel - velikost jadra pro lanczose
     * dest - cilova slozka pro ulozeni vysledku
     */
    int find_annot(string source, string dest, vector<int> kernel, string source2, vector<int> picSizes);
};

#endif // ANNOTATIONFINDER_H
