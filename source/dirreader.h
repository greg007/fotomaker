/*
dirReader - vstup - nazev slozky
             - vystup - seznam obrazku

*/

#ifndef DIRREADER_H
#define DIRREADER_H

#include <string>
#include <vector>
#include <fstream>
#include <dirent.h>
#include <sys/stat.h>
#include <iostream>
#include <locale>

using namespace std;

class dirReader
{
public:
    dirReader();
    /*
     * source - nazev slozky, ze ktere chceme ziskat seznam obrazku
     */
    int dirParse(string source);
    /*
     * index - index obrazku v seznamu
     *
     * return - retezec s nazvem obrazku
     */
    string getPic(int index);
    /*
     * return - pocet obrazku ve slozce
     */
    int getSize();
private:
    vector<string> pics;
};

#endif // DIRREADER_H
