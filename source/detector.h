#ifndef DETECTOR_H
#define DETECTOR_H

#include <string>
#include <vector>
#include <fstream>
#include <dirent.h>
#include <sys/stat.h>
#include <iostream>
#include <locale>
#include "annotationparser.h"
#include "annotationfinder.h"

using namespace std;


class Detector
{
public:
    Detector();
    int detectAnnotation(string source, string dest, vector<int> kernel, vector<int> picSizes);

private:
    int detectAnnotation(string source, string dest, string source2, vector<int> kernel, vector<int> picSizes);
    int with_annot(string source, string annot, string dest, string source2, vector<int> kernel, vector<int> picSizes);
    int without_annot(string source, string dest, string source2, vector<int> kernel, vector<int> picSizes);
    bool detectedAnnotation(string source, string source2);
};

#endif // DETECTOR_H
