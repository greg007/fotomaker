/*
nearest - vstup - uz orezany obrazek obliceje
                     - int velikost_vysledku (24. 26. 28 ...)
         - vystup - prescalovany obrazek

 */

#ifndef NEAREST_H
#define NEAREST_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

class nearest
{
public:
    nearest();
    /*
     * image - vstupni obrazek
     * pict_width - sirka obrazku (24, 26, 28,...)
     * pict_height - vyska obrazku (24, 26, 28,...)
    */
    Mat scale(Mat image, int pict_width, int pict_height);
};

#endif // NEAREST_H
